<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Router
 *
 * @author lover
 */
class Router {

    public $routes = [
        'GET' => [],
        'POST' => []
    ];

    public function get(string $uri, string $controler) {
        $this->routes['GET'][$uri] = $controler;
    }

    public function post(string $uri, string $controler) {
        $this->routes['POST'][$uri] = $controler;
    }

    public function direct(string $uri, string $method) {
        if (array_key_exists($uri, $this->routes[$method])) {
            return $this->routes[$method][$uri];
        }
        
        throw new Exception('No route defined for this URI');
    }
    
    public static function load($file) {
        $router = new static;
        require $file;
        return $router;
    }
}
