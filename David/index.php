<?php

require_once 'autoload.php';

$session = Session::getInstance();
ob_start();

// Constantes de la aplicación
define ('APP_ROOT', __DIR__);


// Si no se provee una ruta, se asignará una por defecto
$route = Request::getInstance()->get('route') ?? '/';

// echo "logged in = " . $session->getVariable('user');


// Creamos una instancia de router, que servirá para navegar a través de las rutas de la aplicación.
// El tema de la autorización se gestionará allí.
$router = new Router();
$router->navigate($route);