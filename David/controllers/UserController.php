<?php

class UserController extends Controller {
    
    /**
     * Obtiene una vista con todos los usuarios
     */
    public function listAllUsers() {
        $users = $this->users->getAllUsers();
        $this->render('/users/list', [
            "title" => "View users",
            "users" => $users
        ]);
        
    }
    
    /**
     * Entra en el panel de usuario conectado
     */
    public function loggedUser() {
        
        if ($this->session->isUserLoggedIn()) {
            $id = $this->session->getVariable("user");
            $user = $this->users->getById($id);
            $userRoles = $this->users->getUserRoles($id);
            $userPerms = $this->users->getUserPerms($id);
            $rolePerms = $this->users->getRolePerms($id);
            
            return $this->render("/users/userDetails", [
                'title' => "Logged User Data",
                'user' => $user,
                'roles' => $userRoles,
                'perms' => $userPerms,
                'role_perms' => $rolePerms,
            ]);
        }
        
        return $this->render("/home/notFound", [
            "title" => "User not found"
        ]);
    }
    
    /**
     * Obtiene la vista del formulario de autenticación o envía los datos de 
     * autenticación al servidor.
     */
    public function login() {
        
        $username = '';
        $password = '';
        
        if ($this->request->isPost()) {
            // Formulario enviado
            $username = $this->request->post('username');
            $password = $this->request->post('password');
            // Buscar credenciales en base de datos
            $user = $this->users->loginUser($username, $password);
            if ($user) {
                // Si logueo exitoso, crear sesión y redirigir
                $this->session::setVariable("user", $user['id']);
                return $this->render('/users/loginSuccessful', [
                    'title' => 'Login Successful'
                ]);
            }
        }
        // Formulario por enviar
        $this->render('/users/loginForm', [
            "title" => "Login form",
            "username" => $username,
            "password" => $password
        ]);

    }
    
    public function logout() {
        if ($this->session->isUserLoggedIn()) {
            $this->session->logoutUser();
        }
        return $this->render('/users/logout', [], [
            "title" => "Logged out"
        ]);
    }
    
    /**
     * Obtiene la vista del formulario de registro o envía los datos de 
     * registro al servidor.
     */
    public function register() {
        
        $username = '';
        $password = '';
        
        
        if ($this->request->isPost()) {
            // Formulario enviado
            $username = $this->request->post('username');
            $password = $this->request->post('password');
            // crear usuario en la base de datos
            $registered = $this->users->registerUser($username, $password);
            if ($registered) {
                // Si registro exitoso, redirigir
                return $this->render('/users/registerSuccessful', [
                    'title' => 'Register Successful'
                ]);
            }
        }
        // Formulario por enviar
        $this->render('/users/registerForm', [
            "title" => "Register form",
            "username" => $username,
            "password" => $password
        ]);

    }
    
}