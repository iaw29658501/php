<?php

class HomeController extends Controller {
    
    
    /**
     * Página de bienvenida
     */
    public function welcome() {
         
        $this->render('/home/home', [
            "title" => "Home page"
        ]);
    }

}