<?php

class UserModel extends Model {
    
    function loginUser($username, $password) {
        return $this->query('select id from users where name = ? and password = ?', 
                [$username, $password], ["RESULT_METHOD" => "fetchArray"]);
    }
    
    
    function registerUser($username, $password) {
        return $this->query("insert into users (name, password) values (?, ?)", 
                [$username, $password], ["RESULT_METHOD" => "affectedRows"]) > 0;
        
    }
    
    function getById($id) {
        return $this->query('SELECT * FROM users WHERE id = ?', $id, 
                ['RESULT_METHOD' => 'fetchArray']);
        
    }
    
    function getUserRoles($id) {
        return $this->query('SELECT user_id, u.name user_name, role_id, r.name role_name FROM user_roles ur join users u on ur.user_id = u.id join roles r on ur.role_id = r.id WHERE user_id = ?', $id, 
                ['RESULT_METHOD' => 'fetchAll']);
        
    }
    
    function getUserPerms($id) {
        return $this->query('SELECT user_id, u.name user_name, perm_id, p.name perm_name FROM user_perms up join users u on up.user_id = u.id join permissions p on up.perm_id = p.id WHERE user_id = ?', $id, 
                ['RESULT_METHOD' => 'fetchAll']);
    }
    
    function getRolePerms($id) {
        return $this->query('SELECT role_id, r.name role_name, perm_id, p.name perm_name FROM role_perms rp join roles r on rp.role_id = r.id join permissions p on rp.perm_id = p.id WHERE role_id = ?', $id, 
                ['RESULT_METHOD' => 'fetchAll']);
    }
    
    function getAllUsers() {
        return $this->query('SELECT * FROM users' , [], 
                ['RESULT_METHOD' => 'fetchAll']);
    }
    
    function getAllRoles() {
        return $this->query("select * from roles", [], ["RESULT_METHOD" => "fetchAll"]);
    }
    
}