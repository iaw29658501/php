<!-- THIS IS THE BASIC TEMPLATE FILE -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?=$params['title']?></title>
        <style>
            
            * {
                margin: 0;
                padding: 0;
                text-decoration: none;
                box-sizing: border-box;
            }
            
            
            header {
                background: #222233;
                color: #dfefff;
                padding: 1em;
                text-align: center;
            }
            
            header h1 {
                font-size: 3em;
            }
            
            header h2 {
                font-size: 2em;
                color: #afbfcf;
            }
            
            nav {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                background: #000;
                padding: 0.5em;
                color: #efefff;
            }
            
            a {
                transition: ease 0.4s all;
                color: #6f6fff;
            }
            
            a:hover {
                color: #6fffff;
            }
            
            p, li, h1, h2, h3 {
                line-height: 1.4em;
            }
            
            nav ul {
                list-style: none;
            }
            
            nav ul li {
                display: inline-block;
                padding: 0.5em;
            }
            
            nav ul li a {
                color: #afafff;
            }
            nav ul li a:hover {
                color: #afffff;
            }
            
            .container {
                width: 90%;
                max-width: 1280px;
                min-width: 400px;
                margin: auto;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>My ACL system</h1>
            <h2><?=$params['title']?></h2>
        </header>
        <nav>
            <!--APP NAVIGATION-->
            <ul class="nav-section">
                <li><a href="<?php echo $_SERVER['PHP_SELF']?>?route=/">Home</a></li>
                <li><a href="<?php echo $_SERVER['PHP_SELF']?>?route=/users/list">Users</a></li>
            </ul>
            <!--USER INFO-->
            <ul class="nav-section">
                <?php if ($this->session->isUserLoggedIn()):?>
                    <li><a href="<?php echo $_SERVER['PHP_SELF']?>?route=/users/me">User logged in</a></li><li><a href="<?php echo $_SERVER['PHP_SELF']?>?route=/logout">Logout</a></li>
                <?php  else:?>
                    You're no logged in? <a href="<?php echo $_SERVER['PHP_SELF']?>?route=/login">Log in here!</a>
                <?php endif;?>
                
            </ul>
        </nav>
        <div class="container">
            <?php include $view ?>
        </div>
    </body>
</html>
