<h2>Home page</h2>
<p>This is the home page content, it means you're in the home page. Use the upper menu bar to navigate.</p>
<p>You could log in or see the user list if you're logged in.</p>
<p>Even if you have a greater role in the organisation maybe you could modify users and roles/permissions.</p>
