<h3>User: <?php echo $params['user']['name']?></h3>
<p>Created at: <?php echo $params['user']['reg_date']?></p>

<h4>Roles:</h4>
<?php if (empty($params['roles'])):?>
None
<?php else: ?>
<ul>
    <?php foreach ($params['roles'] as $role): ?>
    <li><?php echo $role['role_name']?></li>
    <?php endforeach;?>
</ul>
<?php endif;?> 

<h4>Role-defined permissions:</h4>
<?php if (empty($params['role_perms'])):?>
None
<?php else: ?>
<ul>
    <?php foreach ($params['role_perms'] as $perm): ?>
    <li><?php echo $perm['perm_name']?></li>
    <?php endforeach;?>
</ul>
<?php endif;?>

<h4>User-defined permissions:</h4>
<?php if (empty($params['perms'])):?>
None
<?php else: ?>
<ul>
    <?php foreach ($params['perms'] as $perm): ?>
    <li><?php echo $perm['perm_name']?></li>
    <?php endforeach;?>
</ul>
<?php endif; 
