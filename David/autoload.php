<?php

spl_autoload_register(function($className) {
   
   if (preg_match('/.+Model$/', $className)) {
       require_once "model/$className.php";
   } elseif (preg_match('/.+Controller$/', $className)) {
       require_once "controllers/$className.php";
   } else {
       require_once "core/$className.php";
   }
   
});

        