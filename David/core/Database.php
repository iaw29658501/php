<?php

class Database {

    private static $instance = null;
    const RESULT_METHODS =  ['fetchArray', 'fetchAll', 'numRows', 'affectedRows'];
    
    protected $connection;
    protected $query;
    protected $query_count = 0;
    
    private function __construct() {
        $config = parse_ini_file('config/dbconfig.ini');

        $this->connection = new mysqli($config['dbhost'], $config['dbuser'], 
                $config['dbpass'], $config['dbname']);
        $this->exitIfConnectionError('DATABASE CONNECTION ERROR');
        $this->connection->set_charset($config['charset']);
    }

    public function query($query) {
        if ($this->query = $this->connection->prepare($query)) {
            if (func_num_args() > 1) {
                $x = func_get_args();
                $args = array_slice($x, 1);
                $types = '';
                $args_ref = array();
                foreach ($args as $k => &$arg) {
                    if (is_array($args[$k])) {
                        foreach ($args[$k] as $j => &$a) {
                            $types .= $this->_gettype($args[$k][$j]);
                            $args_ref[] = &$a;
                        }
                    } else {
                        $types .= $this->_gettype($args[$k]);
                        $args_ref[] = &$arg;
                    }
                }
                array_unshift($args_ref, $types);
                call_user_func_array(array($this->query, 'bind_param'), $args_ref);
            }
            $this->query->execute();
            $this->exitIfError('Canot process MySQL query (check your params)');
            $this->query_count++;
        } else {
            $this->exitIfConnectionError('Cannot prepare statement (check your syntax)');
        }
        return $this;
    }

    public function fetchAll() {
        $params = array();
        $meta = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array(array($this->query, 'bind_result'), $params);
        $result = array();
        while ($this->query->fetch()) {
            $r = array();
            foreach ($row as $key => $val) {
                $r[$key] = $val;
            }
            $result[] = $r;
        }
        $this->query->close();
        return $result;
    }

    public function fetchArray() {
        $params = array();
        $meta = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array([
            $this->query, 
            'bind_result'
        ], $params);
        $result = [];
        while ($this->query->fetch()) {
            foreach ($row as $key => $val) {
                $result[$key] = $val;
            }
        }
        $this->query->close();
        return $result;
    }

    public function numRows() {
        $this->query->store_result();
        return $this->query->num_rows;
    }

    public function close() {
        return $this->connection->close();
    }

    public function affectedRows() {
        return $this->query->affected_rows;
    }

    private function _gettype($var) {
        if (is_string($var))
            return 's';
        if (is_float($var))
            return 'd';
        if (is_int($var))
            return 'i';
        return 'b';
    }
    
    private function exitIfError($message) {
        if ($this->query->errno) {
            die($message . ': ' . $this->query->error);
        }
    }
    
    private function exitIfConnectionError($message) {
        if ($this->connection->error) {
            die($message . ': ' . $this->connection->error);
        }
    }

    public static function getInstance(): Database {
        if (!self::$instance) {
            self::$instance = new Database();
        }
        return self::$instance;
    }
    
}
