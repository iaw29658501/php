<?php

/**
 * Esta clase tiene como propósito gestionar información sobre la petición HTTP.
 * 
 * Entre otras funciones, cumple el papel de accesor a las variables enviadas
 * con la petición de forma segura.
 * 
 */
class Request {

    private static $instance = null;
    
    private function __construct() {
        
    }
    
    public function isGet() {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }
    
    public function isPost() {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
    
    public function get($variable) {
        if (isset($_GET[$variable])) {
            return $this->sanitizeVariable($_GET[$variable]);
        }
        return null;
    }
    
    public function post($variable) {
        if (isset($_POST[$variable])) {
            return $this->sanitizeVariable($_POST[$variable]);
        }
        return null;
    }
    
    private function sanitizeVariable($variable) {
        return stripslashes(htmlspecialchars(trim($variable))); 
    }
    
    public function getInstance() {
        if (self::$instance === null) {
            self::$instance = new Request();
        }
        return self::$instance;
    }
    
}