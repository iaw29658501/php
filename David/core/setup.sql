use mysql;

drop database if exists acl_test;
create database if not exists acl_test
    character set 'utf8'
    collate 'utf8_spanish_ci';

use acl_test;

create or replace table users (
    id int auto_increment primary key,
    `name` varchar(75) not null unique,
    password varchar(255) not null,
    reg_date timestamp default current_timestamp
);

create or replace table permissions (
    id int auto_increment primary key,
    `name` varchar(75) not null unique
);

create or replace table roles (
    id int auto_increment primary key,
    `name` varchar(75) not null unique
);

create or replace table  role_perms (
    role_id int not null,
    perm_id int not null
);

create or replace table  user_perms (
    user_id int not null,
    perm_id int not null
);

create or replace table  user_roles (
    user_id int not null,
    role_id int not null
);


insert into users (`name`, password) values 
('David', '12skdhjnblk345'),
('Carlos', '45ergewrj6jej78'),
('Pablo', '89aa87ae6ffeefe4'),
('Alexander', '9796skgd46'),
('Erik', '33j93094fj'),
('Cauan', '56467'),
('Mónica', '7964ewcde');

insert into roles (`name`) values 
('usuario'),
('admin');

insert into permissions (`name`) values 
('Ver usuarios'),
('Modificar usuarios');

insert into role_perms values 
(1, 1),
(2, 1),
(2, 2);

insert into user_roles values 
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

insert into user_perms values
(6, 2);
