<?php

class Model {
    
    /**
     * 
     * @param string $query Query statement.
     * @param type $params Query parameters.
     * @param type $options 
     *      RESULT_METHOD -> ['fetchArray', 'fetchAll', 'numRows', 'affectedRows']
     * @return mixed Result of query. With option RESULT_METHOD you can modify 
     *      format or data.
     */
    protected function query($query, $params, $options) {
        $db = Database::getInstance();
        
        if (isset($options['RESULT_METHOD'])) {
            return $db->query($query, $params)->{$options['RESULT_METHOD']}();
        } else {
            return $db->query($query, $params);
        }
    }
    
}