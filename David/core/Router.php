<?php

class Router {
    
    private $routes = [];
    const ROUTES_FILE = APP_ROOT . '/config/routes.json';
    
    public function __construct() {
        $this->routes = json_decode(file_get_contents(self::ROUTES_FILE), true);
    }
    
    public function addRoute($route, $options) {
        $this->routes[$route] = $options;
    }
    
    public function navigate($route) {
        
        if (isset($this->routes[$route]["redirect"])) {
            $redirectRoute = $this->routes[$route]["redirect"];
            // echo "redirection: $redirectRoute";
            return $this->navigate($redirectRoute);
        }
        
        $controller = $this->getControllerInstance($route);
        
        $action = $this->routes[$route]["action"];
        
        return $controller->$action();
        
    }
    
    private function getControllerInstance($route) {
        
        if (isset($this->routes[$route]["controller"])) {
            $controllerClass = $this->routes[$route]["controller"];
            return new $controllerClass();
        } else {
            // Se asume que se usa el controlador por defecto
            return new Controller();
        }
    }
    
}