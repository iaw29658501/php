<?php

class Controller {
    
    protected $request;
    protected $session;
    protected $users;
    
    const VIEW_DIR = APP_ROOT . '/views';
    const TEMPLATE_EXTENSION = '.php';
    const TEMPLATE = self::VIEW_DIR . '/base.template' . self::TEMPLATE_EXTENSION;
    
    public function __construct() {
        $this->request = Request::getInstance();
        $this->session = Session::getInstance();
        $this->users = new UserModel();
    }
    
    /**
     * Muestra la vista correspondiente al archivo especificado.
     * Algunas vistas funcionan con parámetros adicionales.
     * 
     * @param type $viewFileName View file path relative to view directory, without extension.
     * @param type $params Parameters for the view.
     */
    protected function render($viewFileName, $params = []) {
        $view = self::VIEW_DIR . $viewFileName . self::TEMPLATE_EXTENSION;
        
        include_once self::TEMPLATE;
        
    }
}


    
     
 