<?php

class Session {
    
    private static $instance = null;
    
    private function __construct() {
        session_start();
    }
    
    public function setVariable($variable, $value) {
        $_SESSION[$variable] = $value;
    }
    
    public function getVariable($variable) {
        return $this->sanitizeVariable($_SESSION[$variable]);
    }
    
    public function unsetVariable($variable) {
        unset($_SESSION[$variable]);
    }
    
    private function sanitizeVariable($variable) {
        return stripslashes(htmlspecialchars(trim($variable))); 
    }
    
    public function isUserLoggedIn() {
        return boolval($this->getVariable('user'));
    }
    
    public function logoutUser() {
        $this->unsetVariable("user");
    }
    
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new Session();
        }
        return self::$instance;
    }

}