<section>
    <h2>Users list</h2>
    <?php foreach ($params['users'] as $user): ?>
    <article class="user">
        <h3><small><?= $user['name']?></small></h3>
        <p>Created: <time><?= $user['reg_date']?></time></p>
    </article>
    <?php endforeach;?>
</section>