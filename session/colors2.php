<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>


        <?php
        session_start();

        function sanitize($dato) {
                if (is_array($dato)) {
                    $dato = "white";
                } else {
                    $dato = htmlspecialchars(stripslashes(trim($dato)));
                }
                return $dato;
        }

        if ($_SESSION['authenticated'] === 1) {
            $_SESSION['color'] = sanitize($_POST['color']);
        }
        
        header("Location: colors.php");
        
        ?>

    </body>
</html>
