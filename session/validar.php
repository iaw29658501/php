<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>


        <?php
        session_start();

        // The only thing to be authenticated will be having a password length between 4 and 20 characters
        if (isset($_POST['login'])) {
            if (isset($_POST['username']) && isset($_POST['password'])) {
                if (mb_strlen($_POST['password']) >= 4 && mb_strlen($_POST['password']) <= 20) {
                    $_SESSION['authenticated'] = 1;
                    header("Location: colors.php");
                } else {
                    $_SESSION['authenticated'] = 0;
                    $_SESSION['error'] = "the password should have at least 4 characters and 20 as maximum";
                    header("Location: validacio.php");
                }
            }
        }

        ?>

    </body>
</html>
