<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="/cookies.php" method="post">
            <input type="checkbox" name="Ipad1" value="Ipad1"> I have a Ipad1<br>
            <input type="checkbox" name="Ipad2" value="Ipad2"> I have a Ipad2<br>
            <input type="checkbox" name="IpadX" value="IpadX" checked> I have a IpadX<br><br>
            <input type="submit" value="Submit">
        </form>

        <h1> COOKIES </h1>

        <?php
        setcookie("nom", "Cauan", time() + 30);
        setcookie("cognoms", "Goes Mateos", time() + 30);
        setcookie("dni", "11112222Z", time() + 30);

        echo "Hola : " . $_COOKIE['nom'] . " " . $_COOKIE['cognoms'] . "<br>";
        echo "DNI : " . $_COOKIE['dni'];

        // UNSAFE !!! Unserialize is creating a object TOTALLY based on user input
        // We should be encryption and/or using json formater for security!
        $os = array("Linux", "Android", "Mac", "Windows 10");
        setcookie("os", serialize($os), time() + 30);

        echo "<br>OSs :" . implode(" ,", unserialize($_COOKIE['os']));
        
        echo "<br>";
        setcookie("input", serialize($_POST), time()+30);
        
        
        ?>
        <script>
            console.log(document.cookie);
        </script>

    </body>
</html>
