<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Mi primero formulario :)</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script>
                                                /**/
                                                /**/                                                                                                                                                                                                                                   function init() {
/*                    Formulario                  */
                                                /**/                                                                                                                                                                                                                                        function today() {
                                                /**/                                                                                                                                                                                                                                            var today = new Date();
                                                /**/                                                                                                                                                                                                                                             var dd = String(today.getDate()).padStart(2, '0');
                                                /**/                                                                                                                                                                                                                                             var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                                                /**/                                                                                                                                                                                                                                             var yyyy = today.getFullYear();
                                                /**/                                                                                                                                                                                                                                              today = dd + '-' + mm + '-' + yyyy;
                                                /**/                                                                                                                                                                                                                                            return today;
                                                /**/                                                                                                                                                                                                                                           }
/*                     2HIAW                      */
                                                /**/                                                                                                                                                                                                                                          document.getElementById("enrollment").value = today();
                                                /**/
                                                /**/                                                                                                                                                                                                                                       }
/*                                        */
                                                                                                                                                                                                                                                                              window.addEventListener("load", init);
        </script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand">Cauan Goes Mateos</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="form.php">Home</a></li>
                    <li><a href="?g=m">Males</a></li>
                    <li><a href="?g=f">Females</a></li>
                    <li><a href="?g=o">Others</a></li>
                    <li><a href="?g=all">All</a></li>
                </ul>
            </div>
        </nav>

        <?php if (!isset($_GET['g'])) : ?>
            <div class="container">
                <h2> Hola universo</h2>
                <form class="inj" id="form" action="form.php" method="post">
                    <div class="form-group">
                        <label for="dni">DNI:</label>
                        <input type="text" class="form-control" name="dni" placeholder="Ponga tu DNI aqui" required><br>
                    </div>

                    <div class="form-group">
                        <label for="name">Nombre:</label>
                        <input type="text" class="form-control" name="name" placeholder="Ponga tu Nombre aqui" required><br>
                    </div>

                    <div class="form-group">
                        <label for="surname">Apellidos:</label>
                        <input type="text" class="form-control" name="surname" placeholder="Ponga tu apellidos aqui" required><br>
                    </div>

                    <div class="form-group">
                        <label for="address">Direccion:</label>
                        <input type="email" class="form-control" name="email" placeholder="Ponga tu direccion aqui" required><br>
                    </div>

                    <div class="form-group">
                        <label for="postal">Codigo Postal:</label>
                        <input type="text" class="form-control" name="postal" placeholder="Ponga tu codigo postal aqui" required><br>
                    </div>

                    <div class="form-group">
                        <label for="population">Poblacion:</label>
                        <input type="text" class="form-control" name="population" placeholder="Ponga tu pueblo aqui" required><br>
                    </div>

                    <div class="form-group">
                        <label for="tel">Telefono:</label>
                        <input type="tel" class="form-control" name="tel" placeholder="Ponga tu telefono aqui" required><br>
                    </div>

                    <div class="form-group">
                        <label for="gender">Genero:</label>
                        <br>
                        <input type="radio" name="gender" value="male" required> Hombre<br>
                        <input type="radio" name="gender" value="female" required> Mujer <br>
                        <input type="radio" name="gender" value="other" required> Otros

                    </div>

                    <div class="form-group">
                        <label for="language">Lenguas:</label>
                        <br>

                        <input type="checkbox" name="language[]" value="catalan"> Català<br>
                        <input type="checkbox" name="language[]" value="spanish"> Español <br>
                        <input type="checkbox" name="language[]" value="english"> English

                    </div>

                    <div class="form-group">
                        <label for="start">Nacimiento:</label>
                        <input type="text" name="birthday">
                    </div>

                    <!-- poner el today en el value -->
                    <div class="form-group">
                        <label for="enrollment">Matricula:</label>
                        <input type="text" name="enrollment" id="enrollment" value="">
                    </div>

                    <div class="form-group">
                        <label for="observaciones">Observaciones</label><br>
                        <textarea name="observations" ></textarea>
                    </div>

                    <input type="submit" name="submited">
                    <input type='reset' value='Reset' name='reset' onclick="$('#form').trigger('reset');">
                </form>
            </div>
            <h5>
                <?php
            endif;

            if (isset($_POST['submited'])) {
                $right = 0;

                /**
                 * Function which checks if a string is a valid spanish DNI
                 *
                 * @param string $dniUnchecked
                 * @return string $dni
                 */
                function checkDNI($dniUnchecked) {
                    $dniUnchecked = strtoupper($dniUnchecked);
                    //Error-Output
                    $errors = '';
                    //Possible values for the final letter
                    $letterValues = array(
                        'T' => 0, 'R' => 1, 'W' => 2, 'A' => 3, 'G' => 4, 'M' => 5,
                        'Y' => 6, 'F' => 7, 'P' => 8, 'D' => 9, 'X' => 10, 'B' => 11,
                        'N' => 12, 'J' => 13, 'Z' => 14, 'S' => 15, 'Q' => 16, 'V' => 17,
                        'H' => 18, 'L' => 19, 'C' => 20, 'K' => 21, 'E' => 22
                    );

                    //Check if entered
                    if ($dniUnchecked == '' || empty($dniUnchecked)) {
                        $errors .= 'Please enter a DNI.<br/>';
                        echo $errors;
                        return false;
                    }
                    //Check length
                    elseif (strlen($dniUnchecked) != 9) {
                        $errors .= 'Please enter a DNI that has 8 digits and a check-letter.<br/>';
                        echo $errors;
                        return false;
                    }
                    //Check validity
                    elseif (preg_match('/^[0-9]{8}[A-Z]$/i', $dniUnchecked)) {
                        // take numbers as big integer
                        $checkNumber = (int) substr($dniUnchecked, 0, 8);
                        // modulo 23 and check if modulo equals corresponding checkletter
                        if ($checkNumber % 23 == $letterValues[substr($dniUnchecked, 8, 1)]) {
                            //All was ok
                            echo 'dnis is ok<br>';
                            $bdni = true;
                            $dni = trim($dniUnchecked);
                            $dni = stripslashes($dni);
                            $dni = htmlspecialchars($dni);
                            $GLOBALS['right'] ++;
                            return true;
                        } else {
                            $errors .= 'Please enter a valid DNI.<br/>';
                            echo $errors;
                        }
                    }
                }

                checkDNI($_POST['dni']);

                function checkName($nameUnchecked) {
                    // Europe Suport
                    if (preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]{2,50}$/iu', $nameUnchecked)) {
                        echo "Name is good<br>";
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        echo "Please enter a name <br>";
                        return false;
                    }
                }

                checkName($_POST['name']);

                function checkSurname($nameUnchecked) {
                    // Europe Suport
                    if (preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]{2,100}$/iu', $nameUnchecked)) {
                        echo "Surname is good";
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        echo "Please enter a valid surname <br>";
                        return false;
                    }
                }

                checkSurname($_POST['surname']);

                function checkEmail($email) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        echo "Valid email<br>";
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        echo "Please enter a valid email<br>";
                    }
                }

                checkEmail($_POST['email']);

                function checkGender($gender) {
                    if ($gender === "male" || $gender === "female" || $gender === "other") {
                        echo "Valid gender <br>";
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        echo "Please enter a valid gender ('others' is a option)<br>";
                    }
                }

                checkGender($_POST['gender']);

                function checkPostal($postal) {
                    if (preg_match('/\d{5}/', $postal)) {
                        echo "Valid postal<br>";
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        echo "Please enter a valid postal<br>";
                    }
                }

                checkPostal($_POST['postal']);

                function checkTel($tel) {
                    // Solo numeros españoles
                    if (preg_match('/^(\+34|0034|34)?[\s|\-|\.]?[6|7|8|9][\s|\-|\.]?([0-9][\s|\-|\.]?){8}$/', $tel)) {
                        echo "Valid phonenumber<br>";
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        echo "Please enter a valid SPANISH phonenumber<br>";
                    }
                }

                checkTel($_POST['tel']);

                function checkPopulation($population) {
                    if (preg_match('/\w{2,40}/i', $population)) {
                        echo "Valid population<br>";
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        echo "Please enter a valid population<br>";
                    }
                }

                checkPopulation($_POST['population']);

                function checkLanguages($languages) {
                    if (empty($languages)) {
                        $GLOBALS['right'] ++;
                        return true;
                    }

                    foreach ($languages as $value) {
                        if ($value !== "spanish" && $value !== "catalan" && $value === "english") {
                            echo "<br>INVALID language";
                            return false;
                        }
                    }
                }

                checkLanguages($_POST['language']);

                function checkmydate($datex) {
                    $valores = explode("-", $datex);
                    if (count($valores) == 3 && checkdate($valores[1], $valores[0], $valores[2])) {
                        return true;
                    }
                    return false;
                }

                if (checkmydate($_POST['birthday'])) {
                    $GLOBALS['right'] ++;
                } else {
                    echo "<br>INVALID birthday";
                }

                if (checkmydate($_POST['enrollment'])) {
                    $GLOBALS['right'] ++;
                } else {
                    echo "<br>INVALID enrollment";
                }

                function sanitezeAll($dataOrigin) {
                    return array_map(function($dato) {
                        if (is_array($dato)) {
                            $dato = sanitezeAll($dato);
                        } else {
                            $dato = htmlspecialchars(stripslashes(trim($dato)));
                        }
                        return $dato;
                    }, $dataOrigin);
                }

// Todas las informaciones an sido entregues
                echo "<br>" . $right;
                if ($right == 10) {

                    $datos = sanitezeAll($_POST);
                    var_dump($datos);
                    echo "<br><br><br><br>";
                    echo "{";
                    foreach ($datos as $key => $value) {
                        echo "<br> " . $key . " = " . "$value";
                    }

                    echo "<br>}";
                    // $now = new DateTime();
                    $age = 22;

                    try {
                        echo $datos['language'];

                        if (is_array($datos['language'])) {
                            $languages = implode(" ", $datos['language']);
                        } else {
                            $languages = $datos['language'];
                        }

                        $user = "apache";
                        $pass = "password";
                        $db = new PDO('pgsql:dbname=db host=localhost', $user, $pass);
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        // $db->beginTransaction();
                        $sql = "insert into users values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ? ) ";
                        $stmt = $db->prepare($sql);
                        $stmt->execute([$datos['dni'], $datos['name'], $datos['surname'], $datos['email'], $datos['postal'], $datos['population'], $datos['tel'], $datos['gender'], $languages, $datos['birthday'], $age, $datos['enrollment'], null]);
                    } catch (PDOException $ex) {
                        echo "<div class='alert-danger'>{$ex->getMessage()}</div>";
                    }
                    $db = null;
                    $stmt = null;
                }
            }

            class TableRows extends RecursiveIteratorIterator {

                function __construct($it) {
                    parent::__construct($it, self::LEAVES_ONLY);
                }

                function current() {
                    return "<td>" . parent::current() . "</td>";
                }

                function beginChildren() {
                    echo "<tr>";
                }

                function endChildren() {
                    echo "</tr>" . "\n";
                }

            }

            // Si pide enseñare la tabla de los de genero masculino
            if (isset($_GET['g']) && $_GET['g'] === "m") {
                echo "<h2 class='text-center'>Matriculados del genero masculino</h2>";
                echo '<div class="container">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">DNI</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Teléfono</th>
                        </tr>
                    </thead>';
                try {

                    $user = "apache";
                    $pass = "password";
                    $conn = new PDO('pgsql:dbname=db host=localhost', $user, $pass);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare("SELECT dni, nombre, apellidos, telefono FROM users WHERE genero = 'male'");
                    $stmt->execute();
                    // set the resulting array to associative
                    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    foreach (new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k => $v) {
                        echo $v;
                    }
                } catch (PDOException $e) {
                    echo '<div class="alert alert-danger" role="alert">
                            Ha habido un error al cargar los registros, intentelo de nuevo. ' . $e->getMessage() . '
                    </div>';
                }
                $conn = null;
                echo "</table>
            </div>";
            }


            // Si pide enseñare la tabla de los de genero masculino
            if (isset($_GET['g']) && $_GET['g'] === "f") {
                echo "<h2 class='text-center'>Matriculados del genero feminino</h2>";
                echo '<div class="container">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">DNI</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Teléfono</th>
                        </tr>
                    </thead>';
                try {

                    $user = "apache";
                    $pass = "password";
                    $conn = new PDO('pgsql:dbname=db host=localhost', $user, $pass);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare("SELECT dni, nombre, apellidos, telefono FROM users WHERE genero = 'female'");
                    $stmt->execute();
                    // set the resulting array to associative
                    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    foreach (new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k => $v) {
                        echo $v;
                    }
                } catch (PDOException $e) {
                    echo '<div class="alert alert-danger" role="alert">
                            Ha habido un error al cargar los registros, intentelo de nuevo. ' . $e->getMessage() . '
                    </div>';
                }
                $conn = null;
                echo "</table>
            </div>";
            }

            // Si pide enseñare la tabla de los de otros generos
            if (isset($_GET['g']) && $_GET['g'] === "o") {
                echo "<h2 class='text-center'>Matriculados de otros generos</h2>";
                echo '<div class="container">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">DNI</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Teléfono</th>
                        </tr>
                    </thead>';
                try {

                    $user = "apache";
                    $pass = "password";
                    $conn = new PDO('pgsql:dbname=db host=localhost', $user, $pass);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare("SELECT dni, nombre, apellidos, telefono FROM users WHERE genero = 'others'");
                    $stmt->execute();
                    // set the resulting array to associative
                    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    foreach (new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k => $v) {
                        echo $v;
                    }
                } catch (PDOException $e) {
                    echo '<div class="alert alert-danger" role="alert">
                            Ha habido un error al cargar los registros, intentelo de nuevo. ' . $e->getMessage() . '
                    </div>';
                }
                $conn = null;
                echo "</table>
            </div>";
            }

            // Todas las matriculas
            if (isset($_GET['g']) && $_GET['g'] === "all") {
                echo "<h2 class='text-center'>Matriculados del genero feminino</h2>";
                echo '<div class="container">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">DNI</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Teléfono</th>
                        </tr>
                    </thead>';
                try {

                    $user = "apache";
                    $pass = "password";
                    $conn = new PDO('pgsql:dbname=db host=localhost', $user, $pass);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare("SELECT dni, nombre, apellidos, telefono FROM users");
                    $stmt->execute();
                    // set the resulting array to associative
                    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    foreach (new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k => $v) {
                        echo $v;
                    }
                } catch (PDOException $e) {
                    echo '<div class="alert alert-danger" role="alert">
                            Ha habido un error al cargar los registros, intentelo de nuevo. ' . $e->getMessage() . '
                    </div>';
                }
                $conn = null;
                echo "</table>
            </div>";
            }
            ?>

        </h5>

        <script>
            document.getElementById('enrollment').value = (new Date().toDateInputValue()).toString();
        </script>
    </body>
</html>
