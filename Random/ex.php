<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <?php
        echo "Ejercicio 1 <br>";
        $lado = 5;
           function quadrat($lado) {
             $per =  $lado * 4;
             return $per;
        }

        echo "para un quadrado de lado $lado";
        echo "<br>";
        echo "se tiene un perimetro de " . quadrat($lado);

        echo "<br><br>Ejercicio 2 <br> <br>";
        $num1 = 1;
        $num2 = 2;
        $num3 = 3;
        $num4 = 4;
        function ejercicio2($num1, $num2, $num3, $num4) {
            echo "$num1 + $num2 = " . ($num1 + $num2) . "<br>";
            echo "$num3 x $num4 = " . ($num3 * $num4) . "<br>";
        }
        ejercicio2($num1, $num2, $num3, $num4);

        echo "<br><br>Ejercicio 3 <br> <br>";
        $num1 = 1;
        $num2 = 2;
        $num3 = 3;
        $num4 = 4;
        function ejercicio3($num1, $num2, $num3, $num4) {
            echo "$num1 + $num2 + $num3 + $num4  = " . ($num1 + $num2 +$num3 + $num4) . "<br>";
            echo "$num3 x $num4 x $num1 x $num2   = " . ($num3 * $num4 * $num1 * $num2) . "<br>";
        }
        ejercicio3($num1, $num2, $num3, $num4);


        echo "<br><br>Ejercicio 4 <br> <br>";
        $preu = 3.15;
        $cuantitat = 2;
        function ejercicio4($p, $c) {
            echo "Para una cuantitad de  $c con un precio de $p tienes de pagar = " . ( $p * $c ). "<br>";
        }
        ejercicio4($preu, $cuantitat);


        echo "<br><br>Ejercicio 5 <br> <br>";
        $nota1 = 7;
        $nota2 = 8;
        $nota3 = 7;
        function ejercicio5($nota1, $nota2, $nota3) {
            if ( (  $nota1 + $nota2 + $nota3 ) / 3 > 7 ) {
                echo "Tens un promig més alt de 7";
            }
        }
        ejercicio5($nota1, $nota2, $nota3);

        echo "<br><br>Ejercicio 6 <br> <br>";
        $clau1 = 7;
        $clau2 = 7;
        function ejercicio6($clau1, $clau2) {
            if ( $clau1 == $clau2 ) {
                echo "Las claus son iguals ";
            } else {
                echo "Las claus no son iguals ";
            }
        }
        ejercicio6($clau1, $clau2);

        echo "<br><br>Ejercicio 7 <br> <br>";
        $clau1 = 7;
        $clau2 = 8;
        function ejercicio7($clau1, $clau2) {
            if ( $clau1 > $clau2 ) {
                echo "La suma es =  " . ( $clau1 + $clau2) . "<br>";
                echo "La diferencia es =  " . ( $clau1 - $clau2) . "<br>";
            } else {
                echo "Lo producto es =  " . ( $clau1 * $clau2) . "<br>";
                echo "La division es =  " . ( $clau1 / $clau2) . "<br>";
            }
        }
        ejercicio7($clau1, $clau2);

        echo "<br><br>Ejercicio 8 <br> <br>";
        $clau1 = 7;
        $clau2 = 8;
        $clau3 = 5;
        function ejercicio8($clau1, $clau2, $clau3) {
            if ( $clau1 >= $clau2 && $clau1 >= $clau3 ) {
                echo "Lo mayor es $clau1  <br>";
            } elseif ( $clau2 >= $clau1 && $clau2 >= $clau3  ) {
                echo "Lo mayor es $clau2  <br>";
            } elseif ( $clau3 >= $clau1 && $clau3 >= $clau2  ) {
                echo "Lo mayor es $clau3  <br>";
            }
        }
        ejercicio8($clau1, $clau2, $clau3);


                echo "<br><br>Ejercicio 9 <br> <br>";
                $salari = 1000;
                $anys1 = 2;
                $anys2 = 6;
                function ejercicio9($salari, $anys) {
                    if ( $anys >=5 ) {
                        echo "<br>$salari * 1.5 = " . ($salari * 1.5);
                    } else {
                      echo "<br>$salari * 1.$anys = " . ($salari + ($salari * $anys/10));
                    }
                }
                ejercicio9($salari, $anys1);
                ejercicio9($salari, $anys2);


            echo "<br><br>Ejercicio 10 <br> <br>";
            $nums = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14);
            function ejercicio10($nums) {
                $total = 0;
                for ( $i = 0 ; $i < 5; $i++) {
                  $total += $nums[$i];
                }
                echo "<br> total de $total <br>";
            }
            // stupid way -> ejercicio10($nums);
            // easy way
            echo "1 + 2 + 3 + 4 + 5 = " . array_sum(array_slice($nums,0,5)) ." <br>";
              echo "<br><br>Ejercicio 11 <br> <br>";
            echo "4 + 5 + 6 = " . array_sum(array_slice($nums,3,3)) ." <br>";
              echo "<br><br>Ejercicio 12 <br> <br>";
            echo "(3 + 4 + 5 + 6 + 7)/5 = " . array_sum(array_slice($nums,2,5))/5 ." <br>";
             echo "<br><br>Ejercicio 13 <br> <br>";

              $mati = [ 19, 21, 23];
              $tard = [ 18, 24, 23];
              $nit = [ 22, 29, 26];
              echo "Media del primero periodo = " . array_sum($mati)/ sizeof($mati)." <br>" ;
              echo "Media del segundo periodo = " . array_sum($tard)/ sizeof($tard)." <br>"  ;
              echo "Media del tercero periodo = " . array_sum($nit)/ sizeof($nit)." <br>";
              // si tuviera hecho una array de arrays los poderia ordenar facilmente pero esto esta demasiado aburrido :P

               echo "<br><br>Ejercicio 14 <br> <br>";
               function validade($string) {
                 if(filter_var($string, FILTER_VALIDATE_EMAIL)) {
                    // valid address
                    echo "es un email";
                }
                else {
                    echo "no es un email";
                }
               }
               validade("email@server.com");
               echo "<br><br>Ejercicio 15 <br> <br>";
               function validateDate($date) {
                   $test_arr  = explode('-', $test_date);
                    if (count($test_arr) == 3) {
                      if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
                          echo "valid date ...";
                        /*  if ($date == "28-02-2019") {
                            echo "the date is exactly right";
                          }*/
                      } else {
                          echo "problem with dates ...";
                      }
                    } else {
                      echo "problem with input ...";
                    }
                 }
             validateDate("28-02-2019");
  ?>
</body>
</html>
