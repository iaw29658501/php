<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>This should be a title</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <script>
            document.getElementById('enrollment').value = new Date().toDateInputValue();
        </script>

        <div class="container">
            <h2> Para facilitar la correcion $_REQUEST esta siendo ultilizado en lugar de $_POST</h2>
            <form class="inj" id="form" action="form.php" method="post">
                <div class="form-group">
                    <label for="dni">DNI:</label>
                    <input type="text" class="form-control" name="dni" placeholder="Ponga tu DNI aqui" required><br>
                </div>

                <div class="form-group">
                    <label for="name">Nombre:</label>
                    <input type="text" class="form-control" name="name" placeholder="Ponga tu Nombre aqui" required><br>
                </div>

                <div class="form-group">
                    <label for="surname">Apellidos:</label>
                    <input type="text" class="form-control" name="surname" placeholder="Ponga tu apellidos aqui" required><br>
                </div>

                <div class="form-group">
                    <label for="address">Direccion:</label>
                    <input type="email" class="form-control" name="email" placeholder="Ponga tu direccion aqui" required><br>
                </div>

                <div class="form-group">
                    <label for="postal">Codigo Postal:</label>
                    <input type="text" class="form-control" name="postal" placeholder="Ponga tu codigo postal aqui" required><br>
                </div>

                <div class="form-group">
                    <label for="population">Poblacion:</label>
                    <input type="text" class="form-control" name="population" placeholder="Ponga tu pueblo aqui" required><br>
                </div>

                <div class="form-group">
                    <label for="tel">Telefono:</label>
                    <input type="tel" class="form-control" name="tel" placeholder="Ponga tu telefono aqui" required><br>
                </div>

                <div class="form-group">
                    <label for="gender">Genero:</label>
                    <br>
                    <input type="radio" name="gender" value="male" required> Hombre<br>
                    <input type="radio" name="gender" value="female" required> Mujer <br>
                    <input type="radio" name="gender" value="other" required> Otros

                </div>

                <div class="form-group">
                    <label for="language">Lenguas:</label>
                    <br>

                    <input type="checkbox" name="language" value="catalan"> Català<br>
                    <input type="checkbox" name="language" value="spanish"> Español <br>
                    <input type="checkbox" name="language" value="english"> English

                </div>

                <div class="form-group">
                    <label for="start">Nacimiento:</label>
                    <input type="date" name="birthday">
                </div>

                <!-- poner el today en el value -->
                <div class="form-group">
                    <label for="enrollment">Matricula:</label>
                    <input type="date" name="enrollment" id="enrollment" value="">
                </div>

                <input type="submit">
                <input type='reset' value='Reset' name='reset' onclick="$('#form').trigger('reset');">
            </form>
        </div>
        <h5>

            <?php
            pg_connect("host=localhost dbname=dbname user=username password=password")
                    or die("Can't connect to database" . pg_last_error());
            $right = 0;

            /**
             * Function which checks if a string is a valid spanish DNI
             *
             * @param string $dniUnchecked
             * @return string $dni
             */
            function checkDNI($dniUnchecked) {
                $dniUnchecked = strtoupper($dniUnchecked);
                //Error-Output
                $errors = '';
                //Possible values for the final letter
                $letterValues = array(
                    'T' => 0, 'R' => 1, 'W' => 2, 'A' => 3, 'G' => 4, 'M' => 5,
                    'Y' => 6, 'F' => 7, 'P' => 8, 'D' => 9, 'X' => 10, 'B' => 11,
                    'N' => 12, 'J' => 13, 'Z' => 14, 'S' => 15, 'Q' => 16, 'V' => 17,
                    'H' => 18, 'L' => 19, 'C' => 20, 'K' => 21, 'E' => 22
                );

                //Check if entered
                if ($dniUnchecked == '' || empty($dniUnchecked)) {
                    $errors .= 'Please enter a DNI.<br/>';
                    echo $errors;
                    return false;
                }
                //Check length
                elseif (strlen($dniUnchecked) != 9) {
                    $errors .= 'Please enter a DNI that has 8 digits and a check-letter.<br/>';
                    echo $errors;
                    return false;
                }
                //Check validity
                elseif (preg_match('/^[0-9]{8}[A-Z]$/i', $dniUnchecked)) {
                    // take numbers as big integer
                    $checkNumber = (int) substr($dniUnchecked, 0, 8);
                    // modulo 23 and check if modulo equals corresponding checkletter
                    if ($checkNumber % 23 == $letterValues[substr($dniUnchecked, 8, 1)]) {
                        //All was ok
                        echo 'dnis is ok<br>';
                        $bdni = true;
                        $dni = trim($dniUnchecked);
                        $dni = stripslashes($dni);
                        $dni = htmlspecialchars($dni);
                        $GLOBALS['right'] ++;
                        return true;
                    } else {
                        $errors .= 'Please enter a valid DNI.<br/>';
                        echo $errors;
                    }
                }
            }

            checkDNI($_REQUEST['dni']);

            function checkName($nameUnchecked) {
                // Europe Suport
                if (preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]{2,50}$/iu', $nameUnchecked)) {
                    echo "Name is good<br>";
                    $GLOBALS['right'] ++;
                    return true;
                } else {
                    echo "Please enter a name <br>";
                    return false;
                }
            }

            checkName($_REQUEST['name']);

            function checkSurname($nameUnchecked) {
                // Europe Suport
                if (preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]{2,100}$/iu', $nameUnchecked)) {
                    echo "Surname is good";
                    $GLOBALS['right'] ++;
                    return true;
                } else {
                    echo "Please enter a valid surname <br>";
                    return false;
                }
            }

            checkSurname($_REQUEST['surname']);

            function checkEmail($email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    echo "Valid email<br>";
                    $GLOBALS['right'] ++;
                    return true;
                } else {
                    echo "Please enter a valid email<br>";
                }
            }

            checkEmail($_REQUEST['email']);

            function checkGender($gender) {
                if ($gender === "male" || $gender === "female" || $gender === "other") {
                    echo "Valid gender <br>";
                    $GLOBALS['right'] ++;
                    return true;
                } else {
                    echo "Please enter a valid gender ('others' is a option)<br>";
                }
            }

            checkGender($_REQUEST['gender']);

            function checkPostal($postal) {
                if (preg_match('/\d{5}/', $postal)) {
                    echo "Valid postal<br>";
                    $GLOBALS['right'] ++;
                    return true;
                } else {
                    echo "Please enter a valid postal<br>";
                }
            }

            checkPostal($_REQUEST['postal']);

            function checkTel($tel) {
                // Solo numeros españoles
                if (preg_match('/^(\+34|0034|34)?[\s|\-|\.]?[6|7|8|9][\s|\-|\.]?([0-9][\s|\-|\.]?){8}$/', $tel)) {
                    echo "Valid phonenumber<br>";
                    $GLOBALS['right'] ++;
                    return true;
                } else {
                    echo "Please enter a valid SPANISH phonenumber<br>";
                }
            }

            checkTel($_REQUEST['tel']);

            function checkPopulation($population) {
                if (preg_match('/\w{2,40}/i', $population)) {
                    echo "Valid population<br>";
                    $GLOBALS['right'] ++;
                    return true;
                } else {
                    echo "Please enter a valid population<br>";
                }
            }

            checkPopulation($_REQUEST['population']);

            echo "<br>" . "RIGHT: " . $right;

            function checkLanguages($languages) {
                if (empty($languages)) {
                    $GLOBALS['right'] ++;
                    return true;
                }

                foreach ($languages as $value) {
                    if ($value !== "spanish" && $value !== "catalan" && $value === "english") {
                        echo "<br>INVALID language";
                        return false;
                    }
                }
            }

            checkLanguages($_REQUEST['language']);

            if (DateTime::createFromFormat('Y-m-d', $_REQUEST['birthday']) !== FALSE) {
                $GLOBALS['right'] ++;
            } else {
                echo "Please enter a valid date of birthday";
            }

            if (empty($_REQUEST['enrollment'])) {
                $_REQUEST['enrollment'] = date();
            }

// Todas las informaciones an sido entregues

            if ($right == 9) {
                echo "<br><br><br><br>";
                echo "{";
                foreach ($_REQUEST as $key => $value) {
                    echo "<br> " . $key . " = " . "$value";
                }
                echo "<br>}";
            }
            ?>

        </h5>

    </body>
</html>
