\c template1
drop database if exists users;
create database users;
\c users

drop table if exists users cascade;
drop table if exists groups cascade;
drop table if exists groups_users cascade;

create extension citext;
CREATE DOMAIN email AS citext
  CHECK ( value ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$' );

create table users(
	iduser	serial primary key,
	name	varchar(100) not null,
	email	citext unique,
	password text not null
);

create table groups(
	idgroup	serial primary key,
	name	varchar(100),
	description varchar(120)
);

create table groups_users(
	idgroup integer,
	iduser integer,
	CONSTRAINT GROUP_USER_IDGROUP_FK FOREIGN KEY (IDGROUP) REFERENCES GROUPS (IDGROUP),
	CONSTRAINT GROUP_USER_IDUSER_FK FOREIGN KEY (IDUSER) REFERENCES USERS (IDUSER),
	CONSTRAINT GROUP_USER_PK PRIMARY KEY (IDGROUP, IDUSER)
	);



insert into groups (name, description) values ('admins','can do anything');
insert into groups (name, description) values ('basic user','normal ecomerce users, can read and change their accounts');


create extension pgcrypto;

insert into users (name, email, password) values ( 'Cauan', 'cauan1337@protonmail.com',  crypt('password123',gen_salt('md5')));
insert into users (name, email, password) values ( 'David', 'davidm@protonmail.com',  crypt('theleetguy',gen_salt('md5')));
insert into users (name, email, password) values ( 'Carlos', 'carlitos@protonmail.com',  crypt('secureme',gen_salt('md5')));
insert into users (name, email, password) values ( 'Pablo', 'pablitoescobar@protonmail.com',  crypt('gunsandroses',gen_salt('md5')));



grant all privileges on all tables in schema public to "www-data";
