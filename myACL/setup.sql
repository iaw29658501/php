use mysql;

drop database if exists acl_test;
create database if not exists acl_test
    character set 'utf8'
    collate 'utf8_spanish_ci';

use acl_test;

create or replace table users (
    id int auto_increment primary key,
    `username` varchar(75) not null unique,
    password varchar(255) not null,
    email varchar(255) not null unique,
    reg_date timestamp default current_timestamp
);

create or replace table permissions (
    id int auto_increment primary key,
    `name` varchar(75) not null unique
);

create or replace table roles (
    id int auto_increment primary key,
    `name` varchar(75) not null unique
);

create or replace table  role_perms (
    role_id int not null,
    perm_id int not null
);

create or replace table  user_perms (
    user_id int not null,
    perm_id int not null
);

create or replace table  user_roles (
    user_id int not null,
    role_id int not null
);


insert into users (`username`,email, password) values 
('David','david@email.com', SHA('davidpass')),
('Carlos','carlos@email.com', SHA('carlospass')),
('Pablo','pablo@email.com', SHA('pablopass')),
('Alexander','alex@email.com', SHA('alexpass')),
('Erik','erik@email.com', SHA('33j93094fj')),
('Cauan','cauan@email.com', SHA('56467')),
('Mónica','monica@email.com', SHA('7964ewcde'));

insert into roles (`name`) values 
('usuario'),
('admin');

insert into permissions (`name`) values 
('Ver usuarios'),
('Modificar usuarios');

insert into role_perms values 
(1, 1),
(2, 1),
(2, 2);

insert into user_roles values 
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

insert into user_perms values
(6, 2);


CREATE USER IF NOT EXISTS 'dbadmin'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON acl_test.* TO 'dbadmin'@'localhost';

