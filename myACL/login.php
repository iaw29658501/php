<?php
  require "header.php";
  if ($_GET['logout'] == "yes") {
    session_unset();
  }
?>
<?php
  if (!isset($_SESSION['id'])) {
?>
<main>
  <div class="form-group">
    <section>
      <h1>Login</h1>
      <form action="includes/login.inc.php" method="post">
        <input type="text" name="username" id="username" class="form-control" placeholder="username">
        <input type="password" name="password" class="form-control" placeholder="password">
        <input type="submit" name="login-submit" class="form-control" value="submit">
      </form>
    </section>

  </div>
</main>
<?php
} else {
 ?>
<button type="button" onclick="window.location.href='login.php?logout=yes'" class="btn btn-default">Logout</button>

<?php
}
  require "footer.php";
?>
