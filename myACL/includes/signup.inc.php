<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
  if (! isset($_POST['signup-submit']) || $_SERVER['REQUEST_METHOD'] == "GET") {
      header("Location: ../signup.php?error=incorrectMethod");
      exit();
  } else {
      $username = $_POST['username'];
      $email = $_POST['email'];
      $password = $_POST['password'];
      $passwordRepeat = $_POST['passwordRepeat'];

      if (empty($username) || empty($email) || empty($password) || empty($passwordRepeat)) {
          header("Location: ../signup.php?error=emptyFields&email".$email."&username=" . $username);
          exit();
      } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
          header("Location: ../signup.php?error=unvalidFields");
          exit();
      }

      // checking if email make sense
      elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          header("Location: ../signup.php?error=unvalidEmail&username=" . $username);
          exit();
      }
      // validating username
      elseif (!preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
          header("Location: ../signup.php?error=unvalidUsernameCharacters&email".$email);
          exit();
      }
      // checking if the repeated password was right
      elseif ($password !== $passwordRepeat) {
          header("Location: ../signup.php?error=unvalidPasswordRepeat&email".$email."&username=" . $username);
          exit();
      } else {
          require "db.inc.php";
          $sql = "SELECT 1 FROM users WHERE username=?";
          $stmt = mysqli_stmt_init($conn);
          if (!mysqli_stmt_prepare($stmt, $sql)) {
              header("Location: ../signup.php?error=sqlerror");
              exit();
          } else {
              mysqli_stmt_bind_param($stmt, "s", $username);
              mysqli_stmt_execute($stmt);
              mysqli_stmt_store_result($stmt);
              $resultCheck = mysqli_stmt_num_rows($stmt);
              // The username cannot be already in the database
              if ($resultCheck > 0) {
                  header("Location: ../signup.php?error=usernameAlreadyTaken");
                  exit();
              } else {
                  // using bcrypt to hash the password
                  $hashed = password_hash($password, PASSWORD_DEFAULT);
                  $sql = "INSERT INTO users (username, email, password) VALUES (?,?,?)";
                  $stmt = mysqli_stmt_init($conn);
                  if (!mysqli_stmt_prepare($stmt, $sql)) {
                      header("Location: ../signup.php?error=sqlerror");
                      exit();
                  } else {
                      mysqli_stmt_bind_param($stmt, "sss", $username, $email, $hashed);
                      mysqli_stmt_execute($stmt);
                      header("Location: ../signup.php?signup=success");
                      exit();
                  }
              }
          }
          mysqli_stmt_close($stmt);
          mysqli_close($conn);
        }
      }
