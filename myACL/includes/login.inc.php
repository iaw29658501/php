<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
  if (! isset($_POST['login-submit'])) {
      header("Location: ../login.php?error=incorrectMethod");
      exit();
  } else {
      $username = $_POST['username'];
      $password = $_POST['password'];

      // command injection ?

      if (empty($username) || empty($password)) {
          header("Location: ../login.php?error=emptyFields");
          exit();
      }
      if (! preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
          header("Location: ../login.php?error=unvalidUsername");
          exit();
      }
      require "db.inc.php";
      $sql = "SELECT * FROM users WHERE username=? or email=?";
      $stmt = mysqli_stmt_init($conn);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
          header("Location: ../login.php?error=sqlerror");
          exit();
      } else {
          mysqli_stmt_bind_param($stmt, "ss", $username, $username);
          mysqli_stmt_execute($stmt);
          $result = mysqli_stmt_get_result($stmt);
          if ($row = mysqli_fetch_assoc($result)) {
              $passwordCheck = password_verify($password, $row['password']);
              if ($passwordCheck == false) {
                  header("Location: ../login.php?error=wrongPassword");
                  exit();
              } else {
                  session_start();
                  $_SESSION['id'] = $row['id'];
                  header("Location: ../login.php?login=sucess");
                  exit();
              }
          }
      }
      mysqli_stmt_close($stmt);
      mysqli_close($conn);
  }
