<?php
  require "header.php";
?>
<main>
  <div class="form-group">
    <section>
      <h1>Signup</h1>
      <form action="includes/signup.inc.php" method="post">
        <input type="text" name="username" id="username" class="form-control" placeholder="username">
        <input type="email" name="email" class="form-control" placeholder="email">
        <input type="password" name="password" class="form-control" placeholder="password">
        <input type="password" name="passwordRepeat" class="form-control" placeholder="repeat password">
        <input type="submit" name="signup-submit" class="form-control" value="submit">
      </form>
    </section>

  </div>
</main>


<?php
  require "footer.php";
?>
