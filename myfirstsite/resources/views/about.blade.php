@extends('layout')

@section('title', 'about us')

@section('content')
    
<h1>About</h1>

<p>
With lines unseen the land was broken.
When surveyors came, we knew
what the prophet had said was true,
this land with unseen lines would be taken.
</p>
@endsection
